FROM rust:alpine as builder
WORKDIR /usr/src/rust-tutorial
COPY . .
RUN cargo install --path .

FROM alpine:latest
COPY --from=builder /usr/local/cargo/bin/rust-tutorial /usr/local/bin/rust-tutorial
CMD ["rust-tutorial"]