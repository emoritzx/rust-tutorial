APP_NAME := rust-tutorial

image: Dockerfile
	docker build -t $(APP_NAME) .

run:
	docker run --rm -it $(APP_NAME)